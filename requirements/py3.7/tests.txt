#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements/py3.7/tests.txt requirements/tests.in
#
acct==6.5.1
    # via
    #   -r requirements/base.txt
    #   idem
    #   pop-evbus
aiofiles==0.7.0
    # via
    #   acct
    #   dict-toolbox
    #   idem
    #   pop-tree
aiokafka==0.7.2
    # via -r requirements/base.txt
asynctest==0.13.0
    # via
    #   -r requirements/tests.in
    #   pytest-pop
attrs==21.4.0
    # via pytest
cffi==1.15.0
    # via cryptography
colored==1.4.3
    # via rend
cryptography==36.0.1
    # via
    #   acct
    #   pyopenssl
dict-toolbox==2.2.0
    # via
    #   acct
    #   idem
    #   pop
    #   pop-config
    #   pytest-pop
    #   rend
idem==18.7.0
    # via pytest-idem
importlib-metadata==4.10.1
    # via
    #   pluggy
    #   pytest
iniconfig==1.1.1
    # via pytest
jinja2==3.0.3
    # via
    #   idem
    #   rend
jmespath==1.0.0
    # via idem
kafka-python==2.0.2
    # via aiokafka
markupsafe==2.0.1
    # via jinja2
mock==4.0.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
msgpack==1.0.3
    # via
    #   acct
    #   dict-toolbox
    #   pop-evbus
    #   pop-serial
nest-asyncio==1.5.4
    # via
    #   pop-loop
    #   pytest-pop
packaging==21.3
    # via pytest
pluggy==1.0.0
    # via pytest
pop-config==9.0.0
    # via
    #   acct
    #   idem
    #   pop
    #   pytest-pop
pop-evbus==6.1.1
    # via
    #   -r requirements/base.txt
    #   idem
pop-loop==1.0.4
    # via
    #   idem
    #   pop
pop-serial==1.1.0
    # via
    #   idem
    #   pop-evbus
pop-tree==9.2.0
    # via idem
pop==21.0.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-loop
    #   pop-serial
    #   pop-tree
    #   pytest-pop
    #   rend
proxy-tools==0.1.0
    # via pop
py==1.11.0
    # via pytest
pycparser==2.21
    # via cffi
pyopenssl==22.0.0
    # via -r requirements/tests.in
pyparsing==3.0.6
    # via packaging
pytest-async==0.1.1
    # via pytest-pop
pytest-asyncio==0.17.2
    # via
    #   -r requirements/tests.in
    #   pytest-pop
pytest-idem==2.0.0
    # via -r requirements/tests.in
pytest-pop==9.0.0
    # via
    #   -r requirements/tests.in
    #   pytest-idem
pytest==6.2.5
    # via
    #   -r requirements/tests.in
    #   pytest-asyncio
    #   pytest-pop
pyyaml==6.0
    # via
    #   acct
    #   dict-toolbox
    #   idem
    #   pop
    #   rend
rend==6.4.2
    # via
    #   acct
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-tree
sniffio==1.2.0
    # via pop-loop
toml==0.10.2
    # via
    #   idem
    #   pytest
    #   rend
tqdm==4.64.0
    # via idem
typing-extensions==4.0.1
    # via
    #   importlib-metadata
    #   pytest-asyncio
wheel==0.37.1
    # via idem
zipp==3.7.0
    # via importlib-metadata
