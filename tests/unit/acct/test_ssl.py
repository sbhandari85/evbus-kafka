import os
import ssl
import tempfile

import pytest
from OpenSSL import crypto


@pytest.mark.asyncio
async def test_gather(hub):
    acct_key = hub.crypto.fernet.generate_key()

    # Get the current working directory
    cwd = os.getcwd()

    NAME = "evbus-kafka"
    KEY_FILE = f"{NAME}.key"
    CERT_FILE = f"{NAME}.crt"

    # Create data with empty ssl details
    data = {
        "kafka": {
            "my-profile": {
                "connection": {"bootstrap_servers": ["localhost:9093"]},
                "ssl": {"ca_file": "", "key_file": "", "cert_file": "", "password": ""},
            }
        }
    }

    cert_gen(KEY_FILE=KEY_FILE, CERT_FILE=CERT_FILE)

    try:
        data["kafka"]["my-profile"]["ssl"]["ca_file"] = cwd + f"/{CERT_FILE}"
        data["kafka"]["my-profile"]["ssl"]["key_file"] = cwd + f"/{KEY_FILE}"
        data["kafka"]["my-profile"]["ssl"]["cert_file"] = cwd + f"/{CERT_FILE}"
        data["kafka"]["my-profile"]["ssl"]["password"] = NAME

        acct_str = hub.crypto.fernet.encrypt(data=data, key=acct_key)

        with tempfile.NamedTemporaryFile(suffix=".yml.fernet", delete=True) as fh:
            fh.write(acct_str)
            fh.flush()

            # Collect profiles with evbus
            contexts = await hub.evbus.acct.profiles(
                acct_file=fh.name,
                acct_key=acct_key,
            )

        # Perform assertions
        assert "kafka" in contexts
        assert contexts["kafka"]
        assert "my-profile" in contexts["kafka"][0]
        assert "connection" in contexts["kafka"][0]["my-profile"]
        assert "ssl_context" in contexts["kafka"][0]["my-profile"]["connection"]
        assert isinstance(
            contexts["kafka"][0]["my-profile"]["connection"]["ssl_context"],
            ssl.SSLContext,
        )

    finally:
        os.remove(cwd + f"/{CERT_FILE}")
        os.remove(cwd + f"/{KEY_FILE}")


def cert_gen(
    emailAddress="emailAddress",
    commonName="commonName",
    countryName="NT",
    localityName="localityName",
    stateOrProvinceName="stateOrProvinceName",
    organizationName="organizationName",
    organizationUnitName="organizationUnitName",
    serialNumber=0,
    validityEndInSeconds=10 * 365 * 24 * 60 * 60,
    KEY_FILE="private.key",
    CERT_FILE="selfsigned.crt",
):
    # create a key pair
    k = crypto.PKey()
    k.generate_key(crypto.TYPE_RSA, 4096)
    # create a self-signed cert
    cert = crypto.X509()
    cert.get_subject().C = countryName
    cert.get_subject().ST = stateOrProvinceName
    cert.get_subject().L = localityName
    cert.get_subject().O = organizationName
    cert.get_subject().OU = organizationUnitName
    cert.get_subject().CN = commonName
    cert.get_subject().emailAddress = emailAddress
    cert.set_serial_number(serialNumber)
    cert.gmtime_adj_notBefore(0)
    cert.gmtime_adj_notAfter(validityEndInSeconds)
    cert.set_issuer(cert.get_subject())
    cert.set_pubkey(k)
    cert.sign(k, "sha512")
    with open(CERT_FILE, "wt") as f:
        f.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode("utf-8"))
    with open(KEY_FILE, "wt") as f:
        f.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, k).decode("utf-8"))
