import json
import tempfile
import unittest.mock as mock

import pop.hub
import pytest


@pytest.mark.asyncio
async def test_gather():
    hub = pop.hub.Hub()
    hub.pop.sub.add(dyne_name="evbus")

    # Pass extras on the cli with the extra acct information
    extras = json.dumps(
        {
            "kafka": {
                "key": "test_key",
                "partition": "test_partition",
                "topics": ["test_topic1", "test_topic2"],
            }
        }
    )
    with mock.patch("sys.argv", ["acct", f"--extras={extras}", "encrypt", ""]):
        hub.pop.config.load(["acct", "evbus"], cli="acct", parse_cli=True)

    assert "kafka" in hub.OPT.acct.extras

    acct_key = hub.crypto.fernet.generate_key()

    # Create an empty profile to use with the test
    data = {"kafka": {"my_profile": {}}}

    acct_str = hub.crypto.fernet.encrypt(data=data, key=acct_key)

    with tempfile.NamedTemporaryFile(suffix=".yml.fernet", delete=True) as fh:
        fh.write(acct_str)
        fh.flush()

        # Collect profiles with evbus
        contexts = await hub.evbus.acct.profiles(
            acct_file=fh.name,
            acct_key=acct_key,
        )

    # verify that the profile data from acct.extras made it into the profile
    assert contexts == {
        "kafka": [
            {
                "my_profile": {
                    "key": "test_key",
                    "partition": "test_partition",
                    "topics": ("test_topic1", "test_topic2"),
                }
            }
        ]
    }
