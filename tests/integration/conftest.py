from unittest import mock

import aiokafka
import dict_tools.data
import pytest


@pytest.fixture(scope="session")
def acct_profile():
    return "test_development_evbus_kafka"


@pytest.fixture(scope="function", name="hub")
def integration_hub(hub):
    hub.pop.sub.add(dyne_name="evbus")

    with mock.patch("sys.argv", ["evbus"]):
        hub.pop.config.load(["acct", "evbus"], cli="acct")

    yield hub


@pytest.fixture(scope="function")
async def contexts(hub, acct_profile):
    # ACCT_FILE and ACCT_KEY need to be set in the environment
    if not (hub.OPT.acct.acct_file and hub.OPT.acct.acct_key):
        raise pytest.skip(
            f"ACCT_FILE and ACCT_KEY need to be set in the environment to run this test"
        )

    contexts = await hub.evbus.acct.profiles(
        acct_file=hub.OPT.acct.acct_file,
        acct_key=hub.OPT.acct.acct_key,
    )
    if not contexts:
        raise pytest.skip(f"No profiles collected from '{hub.OPT.acct.acct_file}'")
    contexts = dict_tools.data.NamespaceDict(**contexts)
    yield contexts


@pytest.fixture()
async def evbus_broker(hub, contexts):
    task = hub.pop.Loop.create_task(hub.evbus.init.start(contexts))
    await hub.evbus.init.join()

    yield

    # Stop evbus
    await hub.evbus.init.stop()
    await task


@pytest.fixture()
def ctx(contexts, acct_profile):
    profiles = contexts.get("kafka", [])
    for profile in profiles:
        ctx_acct = profile.get(acct_profile)
        if ctx_acct:
            break
    else:
        raise pytest.skip("No profile available for connection")

    yield dict_tools.data.NamespaceDict(acct=ctx_acct)


@pytest.fixture(scope="function")
async def consumer(contexts, acct_profile, ctx):
    try:
        async with aiokafka.AIOKafkaConsumer(
            *ctx.acct.topics, **ctx.acct.connection
        ) as consumer:
            yield consumer
    except Exception as e:
        raise pytest.skip(
            f"Unable to start kafka consumer: {e.__class__.__name__}: {e}"
        )
